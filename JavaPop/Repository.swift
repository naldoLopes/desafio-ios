//
//  Repository.swift
//  JavaPop
//
//  Created by Naldo Lopes on 05/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit
import ObjectMapper

class Repository: NSObject, Mappable {

    var repositoryName : String?
    var repositoryFullName : String?
    var repositoryDescription : String?
    var stargazersCount  : Int?
    var forksCount : Int?
    var openIssuesCount : Int?
    var user : User?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        repositoryName <- map["name"]
        repositoryFullName <- map["full_name"]
        repositoryDescription <- map["description"]
        stargazersCount <- map["stargazers_count"]
        forksCount <- map["forks_count"]
        openIssuesCount <- map["open_issues_count"]
        user <- map["owner"]
    }
}


