//
//  Utility.swift
//  JavaPop
//
//  Created by Naldo Lopes on 06/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit

class Utility: NSObject {

    func showError(message: String, viewController: UIViewController) {
        let alertView = UIAlertController(title: "Atenção", message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertView.addAction(okButton)
        viewController.present(alertView, animated: true, completion: nil)
    }

}
