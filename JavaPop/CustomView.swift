//
//  CustomView.swift
//  JavaPop
//
//  Created by Naldo Lopes on 06/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit

class CustomView: UIView {

    override func awakeFromNib() {
        
        let lineView = UIView(frame:CGRect(x:0, y:43, width:self.frame.size.width, height:1))
        lineView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.addSubview(lineView)
    }

}
