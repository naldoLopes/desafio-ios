//
//  RepositoriesCell.swift
//  JavaPop
//
//  Created by Naldo Lopes on 05/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit

class RepositoriesCell: UITableViewCell {

    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var repositoryDescription: UILabel!
    @IBOutlet weak var repositoryForks: UILabel!
    @IBOutlet weak var repositoryStars: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var ownerImage: UIImageView!
    
    func configureCell(object:Repository) {
        
        self.repositoryName.text = object.repositoryName
        self.repositoryDescription.text = object.repositoryDescription
        self.repositoryForks.text = String(object.forksCount!)
        self.repositoryStars.text = String(object.stargazersCount!)
        self.userName.text = object.user?.userName

        // Cache das imagens utilizando AFNetworking.
        let imageRequest = URLRequest(url:(object.user?.userImageURL)!, cachePolicy:.returnCacheDataElseLoad, timeoutInterval: 60) as URLRequest
        self.ownerImage?.setImageWith(imageRequest , placeholderImage:UIImage(named:"photo_placeholder"), success: nil, failure: nil)

    }
}
