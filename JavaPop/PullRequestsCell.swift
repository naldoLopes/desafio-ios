//
//  PullRequestsCell.swift
//  JavaPop
//
//  Created by Naldo Lopes on 06/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit

class PullRequestsCell: UITableViewCell {

    
    @IBOutlet weak var pullRequestTitle: UILabel!
    @IBOutlet weak var pullRequestBody: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var ownerImage: UIImageView!
  
    func configureCell(object:PullRequest) {
        
        self.pullRequestTitle.text = object.title
        self.pullRequestBody.text = object.body
        self.userName.text = object.user?.userName
        
        // Cache das imagens utilizando AFNetworking.
        let imageRequest = URLRequest(url:(object.user?.userImageURL)!, cachePolicy:.returnCacheDataElseLoad, timeoutInterval: 60) as URLRequest
        self.ownerImage?.setImageWith(imageRequest , placeholderImage:UIImage(named:"photo_placeholder"), success: nil, failure: nil)
    }

}
