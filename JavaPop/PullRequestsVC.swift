//
//  PullRequestsVC.swift
//  JavaPop
//
//  Created by Naldo Lopes on 06/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class PullRequestsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate  {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topInfo: UILabel!
    
    private var pullRequestList = [PullRequest]()
    var repository : Repository!
    
    // MARK: - VIEW LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
        self.title = repository.repositoryName
        self.topInfo.attributedText = self.formatPullRequestStates()
        loadData()
    }
    
    // MARK: - TABLEVIEW DELEGATE & DATA SOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pullRequestList.count == 0 {
            return 0
        } else{
            return pullRequestList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath) as! PullRequestsCell
        let pullRequest = pullRequestList[indexPath.row]
        cell.configureCell(object:pullRequest)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let pullRequest = pullRequestList[indexPath.row]
        
        if let url = pullRequest.htmlUrl {
            UIApplication.shared.openURL(url)
        }
        
        tableView.deselectRow(at:indexPath, animated:true)
    }
    
    // MARK: - DZEMPTY DATASET DELEGATE & DATASOURCE
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named:"empyData")
    }
    
    // MARK: - HELPERS METHODS
    func loadData(){
        
        if let userName = repository.user?.userName, let repositoryName = repository.repositoryName {
            
            DataService.shared.getPullResquestData(userName:userName, repository:repositoryName) { (arrayData, success) in
                
                if success {
                    if(arrayData.count > 0){
                        self.pullRequestList.append(contentsOf:arrayData)
                        self.topInfo.attributedText = self.formatPullRequestStates()
                        self.tableView.reloadData()
                    }
                }
                else{
                    let util = Utility()
                    util.showError(message:"Não foi possivel baixar os dados do servidor!", viewController: self)
                }
            }
        }
    }
    
    func formatPullRequestStates() -> NSAttributedString {
        
        let openStateCount = pullRequestList.filter({$0.state == "open"}).count
        let closeStateCount = pullRequestList.filter({$0.state == "close"}).count

        let openedString = NSAttributedString(string: "opened \(openStateCount)", attributes: [NSForegroundColorAttributeName: UIColor.orange,
                                                                                                  NSFontAttributeName: UIFont.boldSystemFont(ofSize: 15)])
        let closedString = NSAttributedString(string: " / closed \(closeStateCount)", attributes: [NSForegroundColorAttributeName: UIColor.black,
                                                                                                           NSFontAttributeName: UIFont.boldSystemFont(ofSize: 15)])

        let myString = NSMutableAttributedString()
        myString.append(openedString)
        myString.append(closedString)
        return myString
    }
    
}
