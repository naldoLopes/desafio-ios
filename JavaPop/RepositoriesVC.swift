//
//  RepositoriesVC.swift
//  JavaPop
//
//  Created by Naldo Lopes on 05/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class RepositoriesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var repositoryList = [Repository]()
    var lastPage : Bool = false
    var currentPage : Int = 1
    
    // MARK: - NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "pullRequestSegue"{
            if let prvc = segue.destination as? PullRequestsVC, let repository = sender as? Repository {
                    prvc.repository = repository
                    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            }
        }
    }
    
    // MARK: - VIEW LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
        loadData(page: currentPage, showLoader:true)
    }

    // MARK: - TABLEVIEW DELEGATE & DATA SOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if repositoryList.count == 0 {
            return 0
        } else{
            return repositoryList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath) as! RepositoriesCell
        let repository = repositoryList[indexPath.row]
        cell.configureCell(object:repository)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedRepository = repositoryList[indexPath.row] as Repository
        performSegue(withIdentifier:"pullRequestSegue", sender:selectedRepository)
        tableView.deselectRow(at:indexPath, animated:true)
    }
    
    // MARK: - SCROLLVIEW DELEGATE
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard !lastPage else {
            return
        }
        
        // Infinite Scroll Control
        let actualPosition = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height - self.tableView.frame.size.height
        
        if actualPosition >= contentHeight {
            currentPage += 1
            // "message": "Only the first 1000 search results are available",
            // "documentation_url": "https://developer.github.com/v3/search/"
            if currentPage > 1000 {
                lastPage = true
                return
            }
            else
            {
                lastPage = false
            }
            
            let pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            pagingSpinner.startAnimating()
            pagingSpinner.color = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            pagingSpinner.hidesWhenStopped = true
            tableView.tableFooterView = pagingSpinner
            
            loadData(page: currentPage, showLoader: false)
        }
    }
    
    // MARK: - DZEMPTY DATASET DELEGATE & DATASOURCE
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named:"empyData")
    }
    
    // MARK: - HELPERS METHODS
    func loadData(page:Int,showLoader:Bool){
        
        DataService.shared.getRepositoriesData(page: page, showLoader:showLoader) { (arrayData, success) in
            if success {
                if(arrayData.count > 0){
                    self.repositoryList.append(contentsOf:arrayData)
                    self.tableView.reloadData()
                }
            }
            else{
                let util = Utility()
                util.showError(message:"Não foi possivel baixar os dados do servidor!", viewController: self)
            }
        }
    }
}

