//
//  CircularImageView.swift
//  JavaPop
//
//  Created by Naldo Lopes on 06/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit

class CircularImageView: UIImageView {

    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
        self.setNeedsLayout()
    }

}
