//
//  PullRequest.swift
//  JavaPop
//
//  Created by Naldo Lopes on 06/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequest: NSObject, Mappable {
    
    var title: String?
    var body: String?
    var state : String?
    var htmlUrl: URL?
    var user : User?

    required init?(map: Map) {}
    
    // Mappable
    func mapping(map: Map) {
        
        title <- map["title"]
        body <- map["body"]
        state <- map["state"]
        htmlUrl <- (map["html_url"], URLTransform())
        user <- map["user"]
    }

}
