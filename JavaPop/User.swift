//
//  Owner.swift
//  JavaPop
//
//  Created by Naldo Lopes on 06/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import Foundation
import ObjectMapper

class User: NSObject, Mappable {
    
    var userName: String?
    var fullName : String?
    var userImageURL: URL?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        userName <- map["login"]
        userImageURL <- (map["avatar_url"], URLTransform())
    }
    
    
    
    
    
}
