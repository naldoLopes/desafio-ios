//
//  DataService.swift
//  JavaPop
//
//  Created by Naldo Lopes on 06/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import Foundation
import AFNetworking
import ObjectMapper
import SVProgressHUD

class DataService {
    
    private static let baseURL : String  = "https://api.github.com/"
    
    //MARK: Shared Instance
    static let shared : DataService = {
        let instance = DataService()
        return instance
    }()

    //MARK: Download Repositories Data
    func getRepositoriesData(page: Int, showLoader: Bool, completion:@escaping(_ repositoryList: [Repository] ,_ success:Bool) -> Void) {

        
        if showLoader {
            SVProgressHUD.show(withStatus:"Carregando Dados...")
        }
        
        var repositoryList = [Repository]()
        
        repositoryList.removeAll()
        
        let manager = AFHTTPSessionManager(baseURL: URL(string:DataService.baseURL))
        manager.requestSerializer = AFJSONRequestSerializer()
        
        let parameters = ["q" : "language:Java","sort" : "stars","page" : "\(String(page))"]
        
        manager.get("search/repositories", parameters: parameters, progress: nil, success: { (task, responseObject) in

            if let dict = responseObject as? Dictionary<String, AnyObject> {
                
                if let items = dict["items"] as? [[String: AnyObject]]{
                    
                    for item in items {
                        
                        let repository = Mapper<Repository>().map(JSON:item)
                        
                        if repository != nil {
                            repositoryList.append(repository!)
                        }
                    }
                    
                    completion(repositoryList,true)
                    if showLoader {
                        SVProgressHUD.dismiss()
                    }
                }
            }
        }) { (operation, error) in
            
            print(error)
            completion([Repository](), false)
            if showLoader {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    //MARK: Download Pull Requests Data
    func getPullResquestData(userName: String,repository:String ,completion:@escaping(_ pullRequestList:[PullRequest] ,_ success:Bool) -> Void) {
    
        var pullRequestList = [PullRequest]()
        
        SVProgressHUD.show(withStatus:"Carregando Dados...")
        
        let manager = AFHTTPSessionManager(baseURL: URL(string:DataService.baseURL))
        manager.requestSerializer = AFJSONRequestSerializer()
        
        manager.get("repos/\(userName)/\(repository)/pulls", parameters: nil, progress: nil, success: { (task, responseObject) in

            if let array = responseObject as? [Dictionary<String, AnyObject>]{
               
                for item in array {

                    let pullRequest = Mapper<PullRequest>().map(JSON:item)

                    if pullRequest != nil {
                        pullRequestList.append(pullRequest!)
                    }
                }
                
                completion(pullRequestList,true)
                SVProgressHUD.dismiss()
            }

        }) { (operation, error) in
            
            print(error)
            completion([PullRequest](), false)
            SVProgressHUD.dismiss()
        }
    }
}

