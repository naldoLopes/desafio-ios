//
//  UITableViewExtension.swift
//  JavaPop
//
//  Created by Naldo Lopes on 05/10/16.
//  Copyright © 2016 madDog. All rights reserved.
//

import UIKit

extension UITableView {
    
    open override func awakeFromNib() {
        
        let frame = CGRect(x:0, y:0, width: self.frame.size.width, height: self.frame.size.height)
        let backgroundView = UIView(frame: frame)
        
        backgroundView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.backgroundView = backgroundView
    }
    
}
